# Requirements
- python3
- virtualenv (you can skip this if you won't create a separete env, not recommended)
- pip
- Install chrome drivers
- for Windows
```buildoutcfg
Download and install Python3+
```
```buildoutcfg
pip install virtualenv
cd your_project
virtualenv env
\env\Scripts\activate.bat
```
-Download Chromedriver from here (Windows):
https://chromedriver.storage.googleapis.com/index.html?path=83.0.4103.39/
(chromedriver_win32.zip)
```buildoutcfg
Coppy the chromedriver.exe and paste
it in your created env folder.
For example: LocalDisk(C:)\Users\LimeRobot\env\Scripts
```
```

- for Linux
```buildoutcfg
apt install chromium-chromedriver
``` 
- for Mac 
```
brew cask install chromedriver
```


# To run this script you need to:
Create your virtual environment
```
virtualenv limehome -p python3
```
Activate the virtual environment
```
source limehome/bin/activate
```
Install all required packages using pip
```
pip install -r requirements.txt
```
Run the robot framework with activated env (or globally installed packages from requirements.txt file)
```
robot -d results Tests/TestSuit1.robot
```

# Logs
You can check the logs after each test in ./Results/log.html