*** Settings ***
Documentation  Test for hotel booking
Library  Selenium2Library

*** Variables ***
${Browser}  Chrome
#You can use any browser you just need the drivers for it.

${URL}  https://limehome-qa-task.herokuapp.com/
#This is the URL which your browser will oben when you run the command!
${Name}  Ivanov
#This is the variable wich I use for the last name
*** Test Cases ***
Testing for matching the last name in 1st window with the last name of the form.
  Open Browser  ${URL}  ${Browser}
  Maximize Browser Window
  Input Text  id:mat-input-0  ${Name}
  Input Text  id:mat-input-1  123455
  Wait Until Element Contains  class:mat-button  Submit
  Click Element  class:mat-button
Confirm the last names matches.
  Wait Until Page Contains  Your booking details.
  ${actual_value}=  Get Element Attribute  id:mat-input-3  value
  ${Name}=  convert to string  ${Name}
  ${actual_value}=  convert to string  ${actual_value}
  should be equal as strings  ${actual_value}    ${Name}
If The last name match enter date.
  Input Text  id:mat-input-4  02/05/2018
  Click Element  xpath://*[@id="mat-select-0"]
  Wait Until Element Contains  xpath://*[@id="mat-option-38"]  Denmark
  Click Element   xpath://mat-option/span[contains(text(),'Denmark')]
  Click Element  xpath://*[@id="mat-select-1"]
  Wait Until Element Contains  xpath://*[@id="mat-option-161"]  Danish
  Click Element   xpath://mat-option/span[contains(text(),'Danish')]
The test for Booking was successful!
  Close Browser
